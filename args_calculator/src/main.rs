use std::env;
use std::process;
fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 4 {
	std::panic!("invalid argument count");
    }
    let n1: i32 = args[1].parse().expect("invalid number as first arg");
    let n2: i32 = args[3].parse().expect("invalid number as second arg");
    if args[2] == "+" {
	println!("sum of {} and {} is: {}", n1, n2, (n1 + n2));
	process::exit(0);
    } else if args[2] == "-" {
	println!("difference between {} and {} is: {}", n1, n2, (n1-n2));
	process::exit(0);
    } else if args[2] == "*" {
	println!("product between {} and {} is: {}", n1, n2, (n1*n2));
	process::exit(0);
    } else if args[2] == "/" {
	println!("quotient between {} and {} is: {}", n1, n2, (n1/n2));
	process::exit(0);
    }

    std::panic!("enter valid numeric operator as second arg");
}
