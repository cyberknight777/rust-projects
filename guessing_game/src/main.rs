use std::io;
use std::io::{stdout,Write};
use rand::Rng;
use std::cmp::Ordering;

// Simple Guessing Game In Rust!!

fn main() {

    println!("\nWelcome to guessing game in rust!\n");
    
    println!("Guess the number!");

    // makes variable a random integer from range 1-101
    
    let secret_number = rand::thread_rng()
	.gen_range(1, 101);
    
    //    println!("The secret number is: {}", secret_number);

    loop {
	
	print!("Please input your guess: ");

	let _=stdout().flush();
    
	let mut guess = String::new();

	// reads input & flushes stdout from mutable variable guess and uses result type

	io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

//	stdin().read_line(&mut guess).expect("Failed to read line");
	    

	// makes guess a 32-bit integer and matches it with .trim method to check if its Ok or Err
	
	let guess: u32 = match guess.trim()
	    .parse() {
		Ok(num) => num,
		Err(_) => continue,
	    };
//	    .expect("Please type a number!");

// matches guess to secret_number with cmp to check if match expressions arms are matching with either 1 of em
	
	println!("You guessed: {}", guess);
	match guess.cmp(&secret_number) {
	    Ordering::Less => println!(" too small! "),
	    Ordering::Greater => println!(" too big! "),
	    Ordering::Equal => {
		println!(" You win! ");
		break ;

	    }	
	}
    }
}
