# Rust-Projects

## My small-time projects in rust

> Available projects
```
* hello world
* hello cargo
* guessing game
* who are you
* who are you cargo
* branches
* functions
* loops
* vars data types
* fibonacci
* args fibonacci
* calculator
* args calculator
```

> How to use

for projects with cargo use :
```
cargo run
```
for projects without cargo use :
```
rustc main.rs
./main
```
