fn main() {

    println!("Welcome to variables and data types!\n");
    
//mutable / immutable variables. mutable to change its value immutable for constant value

    println!("mutable/immutable variables");
    
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);

//changing variable value with let statement (shadowing)    

    println!("\nchanging variable value by shadowing");
    
    let x = 5;
    
    let x = x + 1;
    let x = x * 2;

    println!("The value of x is: {}", x);

//floating-point types, (numbers with decimal points), f32 & f64

    println!("\nfloating-point types(decimal nums)");
    
    let x = 2.5; // f64
    
    let y: f32 = 3.5; //f32

    println!("x: {}", x);
    println!("y: {}", y);

//numeric operations, + - * / %
    
    println!("\nnumeric operations");
    
    //addition
    let sum = 5 + 10;
    
    //subtraction
    let difference = 95.5 - 4.3;
    
    //multiplication
    let product = 4 * 30;

    //division
    let quotient = 56.7 / 32.2;

    //remainder
    let remainder = 43 % 5;
    
    println!("sum: {}", sum);
    println!("difference: {}", difference);
    println!("product: {}", product);
    println!("quotient: {}", quotient);
    println!("remainder: {}", remainder);

//boolean values (true false)
    
    let _t = true;
    
    let _f: bool = false; // with explicit type annotation

//print more than 1 variables in print statement
    
    println!("\nmore than 1 var in a print statement");

    let a = "hi";
    let b = "man";
    
    println!("{} {} {}", {a}, "my", {b});

//char type, to make vars with unicode characters

    println!("\nchar type, vars with unicode chars");
    
    let c = 'z';
    let z = 'ℤ';
    let crossed_swords = "⚔️";

    println!("c: {}", c);
    println!("z: {}", z);
    println!("crossed_swords: {}", crossed_swords);

//tuple type, group some values with variety of types in 1 compound type
    
    println!("\ntuple type, group some values with variety of types in 1 compound type");

    let _tup: (i32, f64, u8) = (500, 6.4, 1);

    let tup = (500, 6.4, 1);

    let (x, y, z) = tup;

    println!("The value of x: {}", x);
    println!("The value of y: {}", y);
    println!("The value of z: {}", z);

    let x: (i32, f64, u8) = (500, 6.4, 1);

    let five_hundred = x.0;

    let six_point_four = x.1;

    let one = x.2;

    println!("{} {} {}", five_hundred, six_point_four, one);

// array type, collection of multiple values

    println!("\narray type, collection of multiple values");

    let _a = [1, 2, 3, 4, 5];

    let a: [i32; 5] = [1, 2, 3, 4, 5];

    println!("a: {:#?}", a);

    let first = a[0];
    let second = a[1];
    
    println!("first: {}", first);
    println!("second: {}", second);


    let b: [i32; 5] = [3; 5];

    println!("b: {:#?}", b);

//    let index = 10;

//    let element = a[index];

//    println!("the value of element is: {}", element);
   
}
