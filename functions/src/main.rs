fn main() {
    //    println!("Hello, world!");
    
    // calling a function with argument/parameter
    
    //    another_function(8);

    println!("Welcome to functions!\n");

    another_function(8, 10);
    let _x = 5;
    // expressions, block({}) of code in a variable
    let a = {
	let x = 3;
	x + 1
    };

    println!("The value of a is: {}", a);

    let b = five();

    println!("The value of b is: {}", b);

    let c = plus_one(5);

    println!("The value of c is: {}", c);

    println!("Read the source for more information on how functions work!");
}
// declaring another_function with integer types
fn another_function(x: i32, y: i32) {
    //    println!("Another function.");
    println!("The value of x is: {}", x);
    println!("The value of y is: {}", y);
}
// function with return values
fn five () -> i32 {
   5
}
// another example, remember NOT to add semicolon at end of expression as it'll change to a statement and fail as the () in function should be empty tuple
fn plus_one(x: i32) -> i32 {
    x + 1
}
