use std::io;
use std::io::{stdout,Write};

// simple who are you program

fn main() {

    println!("Welcome to who are you program!\n");

// takes input of name and flushes stdout so that there is no trailing newline
    
    print!("Enter your name: ");
    let _=stdout().flush();
    let mut name = String::new();
    // reads input and expects err
        io::stdin()
	.read_line(&mut name)
	.expect("failed to read line xD");
    
// takes input of age and flushes stdout so that there is no trailing newline
    
    print!("Enter your age: ");
    let _=stdout().flush();
    let mut age = String::new();
    // reads input and expects err
    io::stdin()
	.read_line(&mut age)
	.expect("failed to read line xD");

// takes input of favourite programming language and flushes stdout so there is no trailing newline
    
    print!("Enter your favourite programming language: ");
    let _=stdout().flush();
    let mut proglang = String::new();
    // reads input and expects err
    io::stdin()
	.read_line(&mut proglang)
	.expect("failed to read xD");

// takes input of favourite vcs and flushes stdout so there is no trailing newline

    print!("Enter your favourite VCS: ");
    let _=stdout().flush();
    let mut vcs = String::new();
    // reads input and expects err
    io::stdin()
	.read_line(&mut vcs)
	.expect("failed to read xD");
    
    // prints input on screen
    
    println!("\nYour name: {}", name);
    println!("Your age: {}", age);
    println!("Your favourite programming language: {}", proglang);
    println!("Your favourite VCS: {}", vcs);
}
	
    
