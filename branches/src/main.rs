fn main() {

    println!("Welcome to control flow!");
    // if expressions
    let a = 7;
    println!("a: {}", a);
    if a < 5 {
	println!("condition was true");
    } else {
	println!("condition was false");
    }
    //if condition to check integer is not equal to 0
    let b = 2;
    println!("b: {}", b);
    if b != 0 {
	println!("number is not 0");
    }
    //multiple if conditions
    let c = 6;

    if c % 4 == 0 {
	println!("number can be divided by 4");
    } else if c % 3 == 0 {
	println!("number can be divided by 3");
    } else if c % 2 == 0 {
	println!("number can be divided by 2");
    } else {
	println!("number cannot be divided by 4, 3, or 2");
    }
    // if in let statement
    let condition = false;
    let d = if condition { 5 } else { 6 };

    println!("The value of d is: {}", d);
}
