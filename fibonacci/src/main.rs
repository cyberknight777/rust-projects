use std::io;
use std::io::{stdout,Write};
// creates function fibo which takes parameter n as u32 and return type is u32    
fn fibo(n: u32) -> u32 {
    if n <= 0 {
	return 0;
    } else if n == 1 {
	return 1;
    }
    // the fibonacci sequence
    fibo(n - 1) + fibo(n - 2)
}

fn main() {
    println!("Fibonacci generator!");
    // makes a loop to stay in the loop till quit is entered
    loop {
	print!("Enter input (Type \"quit\" to exit script): ");
	// flushes stdout for print statement
	let _=stdout().flush();
	// makes n a mutable variable which is a string in a newline
	let mut n = String::new();
	// reads input
	io::stdin().read_line(&mut n).expect("failed to read line");
	// checks if input is q then it breaks out of the loop
	if n.trim() == "quit" {
	    break;
	}
	// makes n a u32 type which parses n trims the variable
	let n: u32 = n.trim().parse().unwrap();
	// the fibonacci process
	if n == 0 {
	    println!("fibonacci({}) = 0", n);
	} else if n == 1 {
	    println!("fibonacci({}) = 1", n);
	} else {
	    let a: u32 = fibo(n);
	    println!("fibonacci({}) = {}", n, a);
	}
    }
}
