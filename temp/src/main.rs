use std::io;
use std::io::{stdout,Write};


fn main() {
    println!("\nWelcome to temperature converter!\n");
    loop {
	println!("1. Celsius ~> Fahrenheit");
	println!("2. Fahrenheit ~> Celsius");
	println!("3. Exit");
	print!("\nChoose an option: ");
	let _=stdout().flush();
	let mut opt = String::new();
	io::stdin().read_line(&mut opt).expect("Failed to read input");
	let opt: i32 = opt.trim().parse().expect("Failed to read input");
	if opt == 1 {
	    c2f();
	} else if opt == 2 {
	    f2c();
	} else {
	    println!("Exiting..");
	    break
	}
    }
}

fn c2f() {
    print!("Enter temperature in celsius: ");
    let _=stdout().flush();
    let mut temp = String::new();
    io::stdin().read_line(&mut temp).expect("Failed to run");
    let temp: f32 = temp.trim().parse().unwrap();
    let cel = (temp * 9.0)/5.0 + 32.0;
    println!("\nTemperature in fahrenheit: {}\n", cel);
}

fn f2c() {
    print!("Enter temperature in fahrenheit: ");
    let _=stdout().flush();
    let mut temp = String::new();
    io::stdin().read_line(&mut temp).expect("failed to run");
    let temp: f32 = temp.trim().parse().unwrap();
    let fah = (temp - 32.0) * 5.0 / 9.0;
    println!("\nTemperature in celsius: {}\n", fah);
}

