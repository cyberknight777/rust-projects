use std::io;
use std::io::{stdout,Write};

fn main() {
    println!("Hello, welcome to calculator in rust!\n");
    println!("[1]~Addition");
    println!("[2]~Subtraction");
    println!("[3]~Multiplication");
    println!("[4]~Division");
    println!("[5]~Exit");
    loop {
	print!("\ncalc> ");
	let _=stdout().flush();
	let mut opt = String::new();
	io::stdin().read_line(&mut opt).expect("Failed to read input");
	let opt: u32 = opt.trim().parse().unwrap();
	if opt == 1 {
	    addition();
	} else if opt == 2 {
	    subtraction();
	} else if opt == 3 {
	    multiplication();
	} else if opt == 4 {
	    division();
	} else if opt == 5 {
	    break;
	} else {
	    println!("[!] invalid input! exiting script...");
	    break;
	}
    }
}
fn addition() {
    print!("Enter First Digit: ");
    let _=stdout().flush();
    let mut f1 = String::new();
    io::stdin().read_line(&mut f1).expect("invalid input");
    let f1: i32 = f1.trim().parse().unwrap();
    print!("Enter Second Digit: ");
    let _=stdout().flush();
    let mut f2 = String::new();
    io::stdin().read_line(&mut f2).expect("invalid input");
    let f2: i32 = f2.trim().parse().expect("invalid input");
    let fans = f1 + f2;
    println!("The sum of {} and {} is: {}", f1, f2, fans);
	
}
fn subtraction() {
    print!("Enter First Digit: ");
    let _=stdout().flush();
    let mut f1 = String::new();
    io::stdin().read_line(&mut f1).expect("invalid input");
    let f1: i32 = f1.trim().parse().unwrap();
    print!("Enter Second Digit: ");
    let _=stdout().flush();
    let mut f2 = String::new();
    io::stdin().read_line(&mut f2).expect("invalid input");
    let f2: i32 = f2.trim().parse().expect("invalid input");
    let fans = f1 - f2;
    println!("The difference between {} and {} is: {}", f1, f2, fans);
}
fn multiplication() {
    print!("Enter First Digit: ");
    let _=stdout().flush();
    let mut f1 = String::new();
    io::stdin().read_line(&mut f1).expect("invalid input");
    let f1: i32 = f1.trim().parse().unwrap();
    print!("Enter Second Digit: ");
    let _=stdout().flush();
    let mut f2 = String::new();
    io::stdin().read_line(&mut f2).expect("invalid input");
    let f2: i32 = f2.trim().parse().expect("invalid input");
    let fans = f1 * f2;
    println!("The product of {} and {} is: {}", f1, f2, fans);
}
fn division() {
    print!("Enter First Digit: ");
    let _=stdout().flush();
    let mut f1 = String::new();
    io::stdin().read_line(&mut f1).expect("invalid input");
    let f1: i32 = f1.trim().parse().unwrap();
    print!("Enter Second Digit: ");
    let _=stdout().flush();
    let mut f2 = String::new();
    io::stdin().read_line(&mut f2).expect("invalid input");
    let f2: i32 = f2.trim().parse().expect("invalid input");
    let fans = f1 / f2;
    println!("The quotient of {} and {} is: {}", f1, f2, fans);
}
