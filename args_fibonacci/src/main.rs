use std::env;
// import trait env from std lib
fn main() {
    // make args a variable vector of strings which collects args given
    let args: Vec<String> = env::args().collect();
    // make arg a u32 variable which trims and parses args
    let arg: u32 = args[1].trim().parse().unwrap();
    // makes a for loop to check from 0 to the arg and generate the nth number
    for num in 0..arg {
	println!("fibonacci({}) = {}", num, fibo(num));	
    }
}
// makes a function fibo which takes parameter n as u32 and return type is u32
fn fibo(n: u32) -> u32 {
    if n <= 0 {
	return 0;
    } else if n == 1 {
	return 1;
    }
    // the fibonacci sequence process
    fibo(n - 1) + fibo(n - 2)
}
